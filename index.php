
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

<button type="button" class="btn btn-default">button</button>
<br>

<h2>Bài 1</h2>
<?php
function bai1()
{
    for ($i = 0; $i <= 5; $i++) {
        for ($j = 1; $j <= $i; $j++) {
            echo "*";
        }
        echo '<br/>';
    }
}
bai1();
?>

<h2>Bài 2</h2>
<?php
function bai2()
{
    for ($i = 0; $i <= 5; $i++) {
        for ($j = $i + 1; $j <= 5; $j++) {
            echo "*";
        }
        echo '<br/>';
    }
}
bai2();
?>
<h2>Bài 3</h2>
<?php
function bai3()
{
    for ($i = 0; $i < 5; $i++) {
        for ($j = 4; $j >= 0; $j--) {
            if ($j <= $i) {
                echo "*";
            } else
                echo "&nbsp" . "&nbsp";
        }
        echo '<br/>';
    }
}
bai3();
?>
<h2>Bài 4</h2>
<?php
function bai4()
{
    for ($i = 0; $i <= 4; $i++) {
        for ($j = 0; $j <= 4; $j++) {
            if ($j >= $i)
                echo "*";
            else
                echo "&nbsp" . "&nbsp";
        }
        echo '<br />';
    }
}
bai4();
?>
<h2>Bài 5</h2>
<?php
function bai5()
{
    for ($i = 0; $i <= 5; $i++) {
        for ($j = 1; $j <= $i; $j++) {
            echo " " . $j;
        }
        echo '<br/>';
    }
}
bai5();
?>
<h2>Bài 6</h2>
<?php
function bai6()
{
    for ($i = 0; $i <= 5; $i++) {
        for ($j = 5; $j >= 0; $j--) {
            if ($j <= $i) {
                echo $j;
            } else
                echo "&nbsp" . "&nbsp";
        }
        for ($j = 1; $j <= $i; $j++) {
            echo $j;
        }
        echo "<br/>";
    }
}
bai6();

?>
<h2>Bài 7</h2>
<?php
function bai7()
{
    for ($i = 0; $i <= 4; $i++) {
        for ($j = 0; $j <= 4; $j++) {
            if ($i == 0 || $j == 0 || $i == 4 || $j == 4) {
                echo "*";
            } else
                echo "&nbsp" . "&nbsp";
        }
        echo "<br/>";
    }
}
bai7();

?>
<h2>Bài 8</h2>
<?php
function bai8()
{
    for ($i = 0; $i <= 5; $i++) {
        for ($j = 5; $j >= 0; $j--) {
            if ($j <= $i) {
                echo "*";
            } else
                echo "&nbsp" . "&nbsp";
        }
        for ($j = 1; $j <= $i; $j++) {
            echo "*";
        }
        echo "<br/>";
    }
}
bai8();

?>
</body>
</html>



